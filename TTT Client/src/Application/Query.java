package Application;

import java.io.Serializable;

/**
 *
 * @author nouemankhal
 */
public class Query implements Serializable {
    private int type;
    private String content;
    
    public Query(int Type) {
        this.type = Type;
    }
    
    public Query(int Type, String Content) {
        this.type = Type;
        this.content = Content;
    }

    public int getType() {
        return type;
    }

    public String getContent() {
        return content;
    }

}
