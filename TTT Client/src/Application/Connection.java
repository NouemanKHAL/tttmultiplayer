package Application;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nouemankhal
 */
public class Connection {
    static private Socket socket;
    static private boolean isConnected = false;
    
    public Connection() {

    }
    
    public Connection(String ipAddress, int port) {
        try {
            socket = new Socket(ipAddress, port);
            isConnected = true;
        } catch (IOException ex) {
            ex.printStackTrace();
        } 
    }

    public static Socket getSocket() {
        return socket;
    }

    public static boolean isIsConnected() {
        return isConnected;
    }
    
    
}
